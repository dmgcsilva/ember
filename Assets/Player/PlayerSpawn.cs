﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    public RandomGenerator gen;

    public GameObject playerPrefab;

    [System.NonSerialized]
    private bool activated = false;

    void Update()
    {
        if(!activated && gen.finished)
        {
            playerPrefab.SetActive(true);
            activated = true;
        }        
    }
}
