﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ExplosionPropagation : MonoBehaviour
{
    public GameObject funnelExp;
    public float raycastSize = 2.5f;
    public float destroyAfter = 2.5f;
    public float strength = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        ArrayList explosions = new ArrayList();
        Ray ray = new Ray();
        ray.origin = transform.position;

        /*Vertical Raycasts*/
        castTwoDirectionalRays(transform.up, ray, explosions, 'Y');
        /*Horizontal Raycasts x Axis*/
        castTwoDirectionalRays(transform.right, ray, explosions, 'X');
        /*Horizontal Raycasts z Axis*/
        castTwoDirectionalRays(transform.forward, ray, explosions, 'Z');



        foreach (GameObject exp in explosions)
        {
            ParticleSystem ps = exp.GetComponent<ParticleSystem>();
            ps.Play(true);
            Destroy(exp, destroyAfter);
        }

    }

    void castTwoDirectionalRays(Vector3 direction, Ray ray, ArrayList explosions, char axis)
    {
        RaycastHit hit;
        GameObject fExp1, fExp2;

        bool didItHit1 = Physics.Raycast(ray.origin, direction * raycastSize, out hit, raycastSize);
        //Debug.DrawLine(ray.origin, direction * raycastSize, Color.green, 100, true);
        bool didItHit2 = Physics.Raycast(ray.origin, (-direction) * raycastSize, out hit, raycastSize);
        //Debug.DrawLine(ray.origin, (-direction) * raycastSize, Color.green, 100, true);

        if (!didItHit1)
        { // Explosion 1 is a go
            fExp1 = Instantiate(funnelExp, transform.position, Quaternion.LookRotation(direction, Vector3.up)); // Instantiate Explosion 1
            if (!didItHit2)
            { // Explosion 2 is a go
                fExp2 = Instantiate(funnelExp, transform.position, Quaternion.LookRotation(-direction, Vector3.up)); // Instantiate Explosion 2
                explosions.Add(fExp2); // Add Explosion 2 to the list
            }
            else
            {  // Explosion 2 is NOT a go
                addAxisNoise(fExp1, axis);
            }
            explosions.Add(fExp1); // Add Explosion 1 to the list
        }
        else if (!didItHit2)
        {// Explosion 2 is a go but Explosion 1 is NOT a go
            fExp2 = Instantiate(funnelExp, transform.position, Quaternion.LookRotation(-direction, Vector3.up)); // Instantiate Explosion 2
            addAxisNoise(fExp2, axis);
            explosions.Add(fExp2); // Add Explosion 2 to the list
        }
    }

    void addAxisNoise(GameObject explosion, char axis)
    {
        ParticleSystem ps = explosion.GetComponent<ParticleSystem>();
        var noise = ps.noise;
        noise.enabled = true;
        switch (axis)
        {
            case 'X':
                noise.strengthX = strength;
                break;
            case 'Y':
                noise.strengthY = strength;
                break;
            case 'Z':
                noise.strengthZ = strength;
                break;

        }
    }
}
