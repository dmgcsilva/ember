﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickGrenade : MonoBehaviour { 

    public float delay = 5f;
    public GameObject explosionEffect;

    float countdown;
    bool hasExploded = false;
    bool hasCountdownStarted = false;
    Rigidbody rb;


    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
        rb = gameObject.GetComponent<Rigidbody>();
        rb.isKinematic = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (hasCountdownStarted)
            countdown -= Time.deltaTime;

        if (countdown <= 0f && !hasExploded)
        {
            Explode();
            hasExploded = true;
        }
    }

    void Explode()
    {
        GameObject exp = Instantiate(explosionEffect, transform.position, transform.rotation);

        float radius = 5f;
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null) {
                rb.AddExplosionForce(500.0f, explosionPos, radius, 3.0f);
                //Debug.Log("Adding force!");
            }
        }

        Destroy(gameObject);
        Destroy(exp, 2);
    }

    public void StartCountdown()
    {
        hasCountdownStarted = true;
        rb.isKinematic = false;
    }
}
