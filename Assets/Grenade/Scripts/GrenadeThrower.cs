﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeThrower : MonoBehaviour
{
    public float throwForce = 40f;
    public GameObject grenadePrefab;
    public Transform grenadePosition;

    private GameObject grenadeObj;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && StatsScript.nGrenade > 0)
        {
            if (grenadeObj != null)
                  ThrowGrenade();
            else
            {
                grenadeObj = Instantiate(grenadePrefab, grenadePosition.transform.position, grenadePosition.transform.rotation);
                grenadeObj.transform.parent = transform;
            }
        }
    }

    void ThrowGrenade()
    {
        StatsScript.throwG = true;
        grenadeObj.GetComponent<StickGrenade>().StartCountdown();
        Rigidbody rb = grenadeObj.GetComponent<Rigidbody>();
        grenadeObj.transform.SetParent(null);
        rb.AddForce(grenadeObj.transform.forward * throwForce, ForceMode.Impulse);
    }
}
