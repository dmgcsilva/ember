﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public GameObject mainMenu;

    public static bool pressedPlay = false;

    void Update()
    {
        if(!pressedPlay)
        {
            Time.timeScale = 0f;
            mainMenu.SetActive(true);
        }
    }

    public void Play()
    {
        mainMenu.SetActive(false);
        pressedPlay = true;
        Time.timeScale = 1f;
        StatsScript.start = true;
    }

    public void Exit()
    {
        Debug.Log("Exit!");
        Application.Quit();
    }

    public void RestartPlay()
    {
        pressedPlay = false;
    }
}
