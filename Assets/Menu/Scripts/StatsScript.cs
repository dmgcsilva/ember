﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StatsScript : MonoBehaviour
{
    public GameObject stats;
    public Slider slider;
    public Text text;
    public static bool start = false;

    public static bool throwG = false;
    public static bool loseH = false;
    public static int nGrenade;

    int maxHealth = 100;
    int maxGrenade = 20;

    void Update()
    {
        if (Time.timeScale != 0f)
        {
            if (loseH)
            {
                loseHealth();
            }
            if (throwG && int.Parse(text.text)>0)
            {
                throwGrenade();
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                resetGrenade();
            }
        }
        if (slider.value <= 0)
        {
            Debug.Log("You are dead! Good luck next time.");
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            MainMenuScript.pressedPlay = false;
        }
        if(start)
        {
            resetGrenade();
            resetHealth();
            start = false;
        }

        if (!MainMenuScript.pressedPlay || ExplorerMode.exploreActive)
        {
            stats.SetActive(false);
        }
        else
        {
            stats.SetActive(true);
        }
    }

    public void loseHealth()
    {
        slider.value = slider.value-5;
        loseH = false;
    }

    public void throwGrenade()
    {
        text.text = (int.Parse(text.text)-1).ToString();
        throwG = false;
        nGrenade--;
    }

    public void resetHealth()
    {
        slider.maxValue = maxHealth;
        slider.value = maxHealth;
    }

    public void resetGrenade()
    {
        text.text = maxGrenade.ToString();
        nGrenade = maxGrenade;
    } 

}
