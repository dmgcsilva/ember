﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplorerMode : MonoBehaviour
{
    public GameObject camera;
    public GameObject pauseMenuUI;

    public static bool exploreActive = false;

    void Start()
    {
        camera.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) & exploreActive)
        {
            camera.SetActive(false);            
            exploreActive = false;
        }
    }

    public void Explorer()
    {
        exploreActive = true;
        camera.SetActive(true);
        pauseMenuUI.SetActive(false);
        Time.timeScale = 0f;
        PauseMenuScript.isPaused = false;
    }
}
