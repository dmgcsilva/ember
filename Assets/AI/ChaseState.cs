﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIStateMachine;

public class ChaseState : State<AI>
{

    private static ChaseState _instance;

    public ChaseState()
    {
        if (_instance != null)
            return;
        _instance = this;
    }

    public static ChaseState Instance
    {
        get
        {
            if (_instance == null)
                new ChaseState();
            return _instance;
        }
    }

    public override void EnterState(AI _o)
    {
        //Debug.Log("Entering Chase State.");
    }

    public override void ExitState(AI _o)
    {
        //Debug.Log("Exiting Chase State.");
    }

    public override void Update(AI _o)
    {
        if (_o.switchState)
        {
            _o.stateMachine.ChangeState(AttackState.Instance);
        } else
        {

            float speed = _o.runSpeed;
            _o.anim.SetFloat("Speed", speed);
        }
    }
}
