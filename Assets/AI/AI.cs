﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIStateMachine;
using UnityEngine.AI;

public class AI : MonoBehaviour
{
    [HideInInspector]public bool switchState = false;
    [HideInInspector] public float turnSpeed = 10f;

    private float groundCheckDistance = 0.25f;
    private bool isGrounded;

    [HideInInspector] public float walkSpeed = 0.2f;
    [HideInInspector] public float runSpeed = 0.75f;
    [HideInInspector] public Vector3 targetDirection;
    [HideInInspector] public float lastSlash = 100f;

    public Animator anim;
    public NavMeshAgent navMeshAgent;

    [HideInInspector] public Vector3 targetPoint;
    [HideInInspector] public GameObject destination;
    [HideInInspector] public GameObject previousDestination;
    [HideInInspector] public StatsScript dmgController; 

    public StateMachine<AI> stateMachine { get; set; }

    private void Start()
    {
        anim = GetComponent<Animator>();
        stateMachine = new StateMachine<AI>(this);
        stateMachine.ChangeState(PatrolState.Instance);
        navMeshAgent = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (navMeshAgent == null)
            Debug.Log("No NavMeshAgent attached");

        GameObject menu = GameObject.FindGameObjectsWithTag("Menu")[0];
        if (menu != null)
            dmgController = menu.GetComponent<StatsScript>();
    }

    private void FixedUpdate()
    {
        // use a point of view obj to detect the player(since they smell instead of seeing use a sphere)
        // if player is in the sphere then switchState = true
        // else remain in the same state

        LookForPlayer();
        stateMachine.Update();


        navMeshAgent.SetDestination(targetPoint);
                

        CheckGroundStatus();
        anim.SetBool("OnGround", isGrounded);

    }

    private void LookForPlayer()
    {
        float radius = 10f;
        Vector3 npcPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(npcPos, radius);
        bool found = false;
        foreach (Collider hit in colliders)
        {
            // check if is player
            if (hit.gameObject.GetComponent<Player>() != null || hit.gameObject.tag == "Player")
            {
                targetPoint = hit.transform.position;
                float dist = Vector3.Magnitude(targetPoint - transform.position);
                if (dist < 1.5f)
                {
                    if (!(stateMachine.currentState is AttackState))
                        stateMachine.ChangeState(AttackState.Instance);
                }
                else if (!(stateMachine.currentState is ChaseState))
                    stateMachine.ChangeState(ChaseState.Instance);
                found = true;
            }
        }
        if (!found && !(stateMachine.currentState is PatrolState))
            stateMachine.ChangeState(PatrolState.Instance);
    }

    void CheckGroundStatus()
    {
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, groundCheckDistance))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }

    //Trigger Kirin Audio Effects
    //These are called by Events in each animation file. The names need to match the ones set there.
    //If the name is changed, the Event in the animation file needs to be changed as well.
    void IdlePant() => KirinAudioPlayer.instance.PlayAudioIdlePant();
    void IdleCaution() => KirinAudioPlayer.instance.PlayAudioIdleCaution();
    void IdleClean() => KirinAudioPlayer.instance.PlayAudioIdleClean();
    void WalkForward() => KirinAudioPlayer.instance.PlayAudioWalkForwardFootSteps();
    void RunForward() => KirinAudioPlayer.instance.PlayAudioRunForwardFootSteps();
    void Jump() => KirinAudioPlayer.instance.PlayAudioJump();
    void JumpLand() => KirinAudioPlayer.instance.PlayAudioJumpLand();
    void Slash() => KirinAudioPlayer.instance.PlayAudioSlash();
}
