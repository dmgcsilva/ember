﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamesParticles : MonoBehaviour
{
    public ParticleSystem stream;
    public ParticleSystem heatDistortion;
    public float distortionFade;

    private Material distortionMaterial;
    private bool turningOffDistortion = false;
    private float acum = 0;
    private float initialDistortion;

    void Start()
    {
        distortionMaterial = heatDistortion.GetComponent<ParticleSystemRenderer>().material;
        initialDistortion = distortionMaterial.GetFloat("_BumpAmt");
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            stream.Play();
            heatDistortion.Play();
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            stream.Stop();
            heatDistortion.Stop();
            turningOffDistortion = true;
        }

        if (turningOffDistortion)
            turnOffDistortion();
    }

    void turnOffDistortion()
    {
        if (acum < 1)
        {
            acum += Time.deltaTime / distortionFade;
        }

        float distortion = Mathf.Lerp(distortionMaterial.GetFloat("_BumpAmt"), 0, acum);
        distortionMaterial.SetFloat("_BumpAmt", distortion);
        if (distortion <= 0.05)
        {
            turningOffDistortion = false;
            acum = 0;
            heatDistortion.Clear();
            distortionMaterial.SetFloat("_BumpAmt", initialDistortion);
        }
    }
}
