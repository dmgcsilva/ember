﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamesAudio : MonoBehaviour
{
    public AudioSource audioSource;

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            audioSource.Play();
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            audioSource.Stop();
        }
    }
}
