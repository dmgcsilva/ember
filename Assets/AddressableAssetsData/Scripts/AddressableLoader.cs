﻿
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;

public static class AddressableLoader
{

#if UNITY_EDITOR
    public static T LoadAssetInEditor<T>(AssetReference reference)
        where T : Object
    {
        if (reference == null)
            return null;

        string guid = getGUID(reference);
        string path = AssetDatabase.GUIDToAssetPath(guid);
        var asset = AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
        return asset;
    }
#endif

    public static string getGUID(AssetReference reference)
    {
        return reference.RuntimeKey.ToString();
    }
}
