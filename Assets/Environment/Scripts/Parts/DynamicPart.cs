﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


[System.Serializable]
public struct Shape
{
    [SerializeField, HideInInspector] private string name;
    [SerializeField, HideInInspector] private List<GameObject> elements;

    public Shape(string name, List<GameObject> elements) : this()
    {
        this.Name = name;
        this.Elements = elements;
    }

    public string Name { get => name; private set => name = value; }
    public List<GameObject> Elements { 
        get => elements != null ? new List<GameObject>(elements) : new List<GameObject>(); 
        private set => elements = value != null ? new List<GameObject>(value) : null; 
    }
}

public class DynamicPart : MonoBehaviour
{
    [SerializeField, HideInInspector] private List<Shape> shapes;

    public List<Shape> Shapes { 
        get => shapes != null ? new List<Shape>(shapes) : new List<Shape>(); 
        private set => shapes = value != null ? new List<Shape>(value) : null; 
    }

    public HashSet<string> getShapeNames()
    {
        HashSet<string> shapes = new HashSet<string>();
        foreach (Shape shape in this.shapes)
        {
            shapes.Add(shape.Name);
        }
        return shapes;
    }

    public List<GameObject> getElements(string shapeName)
    {
        if (shapeName != null)
        {
            foreach (Shape shape in shapes)
            {
                if (shapeName.Equals(shape.Name))
                {
                    return shape.Elements;
                }
            }
        }
        return new List<GameObject>();
    }





#if UNITY_EDITOR


    [System.NonSerialized, ShowInInspector, HideLabel, HideReferenceObjectPicker, EnableIf("@inspector != null && inspector.editMode")]
    private Inspector inspector;

    [OnInspectorGUI]
    private void updateInspector()
    {
        if (inspector == null)
            inspector = new Inspector(this);
    }

    [DisableIf("@inspector != null && inspector.editMode"), Button]
    private void edit()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = true;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void save()
    {
        inspector.reset();
        inspector.save();
        inspector.editMode = false;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void cancel()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = false;
    }



    private class Inspector
    {
        [HideInInspector]
        public bool editMode;

        [HideInInspector]
        public DynamicPart target;

        public Inspector(DynamicPart target)
        {
            this.editMode = false;
            this.target = target;
            reset();
            sync();
        }

        public void reset()
        {

        }

        public void sync()
        {
            shapes = target.Shapes.Select(t => new _Shape(t)).ToList();
        }

        public void save()
        {
            target.Shapes = shapes.Select(t => t.convert()).ToList();
        }

        [ListDrawerSettings(Expanded = true)]
        public List<_Shape> shapes;

        public struct _Shape
        {
            [LabelWidth(40)]
            public string name;

            [PropertySpace(SpaceBefore = 5)]
            [ValueDropdown("@EnvironmentUtils.getAllPartElements(($property.Tree.WeakTargets[0] as DynamicPart).gameObject)", ExcludeExistingValuesInList = true)]
            public List<GameObject> elements;

            public _Shape(string name, List<GameObject> elements)
            {
                this.name = name;
                this.elements = elements;
            }

            public _Shape(Shape shape)
            {
                this.name = shape.Name;
                this.elements = shape.Elements;
            }

            public Shape convert()
            {
                return new Shape(name, elements);
            }
        }
    }

#endif
}