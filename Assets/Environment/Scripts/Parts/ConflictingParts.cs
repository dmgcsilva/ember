﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum Settlement { First, Last, A, B };

[System.Serializable]
public struct Conflict
{
    [SerializeField, HideInInspector] private GameObject elementA;
    [SerializeField, HideInInspector] private GameObject elementB;
    [SerializeField, HideInInspector] private Settlement settlement;

    public Conflict(GameObject elementA, GameObject elementB, Settlement settlement) : this()
    {
        ElementA = elementA;
        ElementB = elementB;
        Settlement = settlement;
    }

    public GameObject ElementA { get => elementA; private set => elementA = value; }
    public GameObject ElementB { get => elementB; private set => elementB = value; }
    public Settlement Settlement { get => settlement; private set => settlement = value; }

    public Conflict inverse()
    {
        return new Conflict(elementB,elementA,settlement);
    }
}

public class ConflictingParts : MonoBehaviour
{
    [SerializeField, HideInInspector] private List<GameObject> disabledElements = default;
    [SerializeField, HideInInspector] private List<Conflict> equivalentElements = default;

    public List<GameObject> DisabledElements { 
        get => disabledElements != null ? new List<GameObject>(disabledElements) : new List<GameObject>(); 
        private set => disabledElements = value != null ? new List<GameObject>(value) : null; 
    }
    public List<Conflict> EquivalentElements { 
        get => equivalentElements != null ? new List<Conflict>(equivalentElements) : new List<Conflict>(); 
        private set => equivalentElements = value != null ? new List<Conflict>(value) : null; 
    }





#if UNITY_EDITOR


    [System.NonSerialized, ShowInInspector, HideLabel, HideReferenceObjectPicker, EnableIf("@inspector != null && inspector.editMode")]
    private Inspector inspector;

    [OnInspectorGUI]
    private void updateInspector()
    {
        if (inspector == null)
            inspector = new Inspector(this);
    }

    [DisableIf("@inspector != null && inspector.editMode"), Button]
    private void edit()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = true;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void save()
    {
        inspector.reset();
        inspector.save();
        inspector.editMode = false;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void cancel()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = false;
    }



    private class Inspector
    {
        [HideInInspector]
        public bool editMode;

        [HideInInspector]
        public ConflictingParts target;

        public Inspector(ConflictingParts target)
        {
            this.editMode = false;
            this.target = target;
            reset();
            sync();
        }

        public void reset()
        {
            this.partName = default;
            this.elementName = default;
            this.settlementEquivalentElements = default;
            this.sensitivityDetectEquivalentElements = 0.01f;
        }

        public void sync()
        {
            disabledElements = target.DisabledElements;
            equivalentElements = target.EquivalentElements.Select(t => new Inspector._Conflict(t)).ToList();
        }

        public void save()
        {
            target.DisabledElements = disabledElements;
            target.EquivalentElements = equivalentElements.Select(i => i.convert()).ToList();
        }

        [PropertyOrder(0)]
        [BoxGroup("Disable Elements")]
        [LabelWidth(100)]
        public string partName;

        [PropertyOrder(1)]
        [BoxGroup("Disable Elements")]
        [LabelWidth(100)]
        public string elementName;

        [PropertySpace(5)]
        [PropertyOrder(2)]
        [BoxGroup("Disable Elements")]
        [Button("Disable")]
        public void disableElementsNamedWithSubstring()
        {
            foreach (Transform part in target.transform)
            {
                string partName = part.name.Split(new string[] { " (" }, StringSplitOptions.None)[0];
                if (partName.Equals(this.partName))
                {
                    foreach (Transform element in part)
                    {
                        string elementName = element.name.Split(new string[] { " (" }, StringSplitOptions.None)[0];
                        if (elementName.Equals(this.elementName))
                        {
                            disabledElements.Add(element.gameObject);
                        }
                    }
                }
            }
        }

        [PropertySpace(10)]
        [PropertyOrder(3)]
        [BoxGroup("Disable Elements")]
        [LabelText("Elements")]
        [ListDrawerSettings(HideAddButton = true)]
        public List<GameObject> disabledElements;

        [PropertyOrder(4)]
        [BoxGroup("Equivalent Elements")]
        [LabelText("Settlement")]
        public Settlement settlementEquivalentElements;

        [PropertyOrder(5)]
        [BoxGroup("Equivalent Elements")]
        [LabelText("Sensitivity")]
        public float sensitivityDetectEquivalentElements;

        [PropertySpace(5)]
        [PropertyOrder(6)]
        [BoxGroup("Equivalent Elements")]
        [Button("Detect")]
        public void detectEquivalentElements()
        {
            equivalentElements = new List<_Conflict>();
            for (int i = 0; i < target.transform.childCount; i++)
            {
                Transform part = target.transform.GetChild(i);
                string partName = part.name.Split(new string[] { " (" }, StringSplitOptions.None)[0];
                for (int j = i + 1; j < target.transform.childCount; j++)
                {
                    Transform otherPart = target.transform.GetChild(j);
                    string otherPartName = part.name.Split(new string[] { " (" }, StringSplitOptions.None)[0];
                    for (int pi = 0; pi < part.childCount; pi++)
                    {
                        if (partName.Equals(otherPartName))
                        {
                            Transform element = part.GetChild(pi);
                            string elementName = element.name.Split(new string[] { " (" }, StringSplitOptions.None)[0];
                            for (int pj = 0; pj < otherPart.childCount; pj++)
                            {
                                Transform otherElement = otherPart.GetChild(pj);
                                string otherElementName = otherElement.name.Split(new string[] { " (" }, StringSplitOptions.None)[0];
                                if (elementName.Equals(otherElementName) && Vector3.Distance(element.position, otherElement.position) < .01)
                                {
                                    equivalentElements.Add(new _Conflict(element.gameObject, otherElement.gameObject, settlementEquivalentElements));
                                }
                            }
                        }
                    }
                }
            }
        }

        [PropertySpace(10)]
        [PropertyOrder(7)]
        [BoxGroup("Equivalent Elements")]
        [LabelText("Elements")]
        public List<_Conflict> equivalentElements;

        public struct _Conflict
        {
            public GameObject elementA;
            public GameObject elementB;
            public Settlement settlement;

            public _Conflict(GameObject elementA, GameObject elementB, Settlement settlement)
            {
                this.elementA = elementA;
                this.elementB = elementB;
                this.settlement = settlement;
            }

            public _Conflict(Conflict target)
            {
                this.elementA = target.ElementA;
                this.elementB = target.ElementB;
                this.settlement = target.Settlement;
            }

            public Conflict convert()
            {
                return new Conflict(elementA, elementB, settlement);
            }
        }
    }

#endif
}
