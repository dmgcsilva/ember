﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UnityEngine;


[CreateAssetMenu(fileName = "MappingPortPart", menuName = "Procedural/Ports/Mapping")]
public class MappingPortPart : ScriptableObject
{
    [SerializeField, HideInInspector] private PortType type;
    [SerializeField, HideInInspector] private GameObject part;
    [SerializeField, HideInInspector] private List<Entry> entrys;

    public MappingPortPart(PortType type, GameObject part, List<Entry> entrys)
    {
        this.Type = type;
        this.Part = part;
        this.Entrys = entrys;
    }

    public PortType Type { get => type; private set => type = value; }
    public GameObject Part { get => part; private set => part = value; }

    public List<Entry> Entrys
    {
        get => entrys != null ? new List<Entry>(entrys) : new List<Entry>();
        private set => entrys = value != null ? new List<Entry>(value) : null;
    }


    [System.Serializable]
    public struct Entry
    {
        [SerializeField, HideInInspector] private Plug plug;
        [SerializeField, HideInInspector] private string shape;

        public Entry(Plug plug, string shape) : this()
        {
            this.Plug = plug;
            this.Shape = shape;
        }

        public Plug Plug { get => plug; private set => plug = value; }
        public string Shape { get => shape; private set => shape = value; }
    }





#if UNITY_EDITOR


    [System.NonSerialized, ShowInInspector, HideLabel, HideReferenceObjectPicker, EnableIf("@inspector != null && inspector.editMode")]
    private Inspector inspector;

    [OnInspectorGUI]
    private void updateInspector()
    {
        if (inspector == null)
            inspector = new Inspector(this);
    }

    [DisableIf("@inspector != null && inspector.editMode"), Button]
    private void edit()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = true;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void save()
    {
        inspector.reset();
        inspector.save();
        inspector.editMode = false;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void cancel()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = false;
    }



    private class Inspector
    {
        [HideInInspector]
        public bool editMode;

        [HideInInspector]
        public MappingPortPart target;

        public Inspector(MappingPortPart target)
        {
            this.editMode = false;
            this.target = target;
            reset();
            sync();
        }

        public void reset()
        {

        }

        public void sync()
        {
            this.type = target.Type;
            this.part = target.Part;
            entrys = target.Entrys.Select(t => new _Entry(t)).ToList();
        }

        public void save()
        {
            target.Type = type;
            target.Part = part;
            target.Entrys = entrys.Select(t => t.convert()).ToList();
        }

        [ValueDropdown("@EnvironmentUtils.getAssetDropdown<PortType>()")]
        public PortType type;

        [ValueDropdown("@EnvironmentUtils.getAssetsInPath(EnvironmentUtils.partsPath)")]
        public GameObject part;

        [EnableIf("@type != null && part != null")]
        public List<_Entry> entrys;


        public struct _Entry
        {
            [ValueDropdown("@getPlugs($property.Tree.WeakTargets[0] as MappingPortPart)")]
            public Plug plug;

            [ValueDropdown("@getShapes($property.Tree.WeakTargets[0] as MappingPortPart)")]
            public string shape;

            public _Entry(Plug plug, string shape) : this()
            {
                this.plug = plug;
                this.shape = shape;
            }

            public _Entry(Entry target)
            {
                this.plug = target.Plug;
                this.shape = target.Shape;
            }

            public Entry convert()
            {
                return new Entry(plug, shape);
            }

            public IEnumerable getPlugs(MappingPortPart target)
            {
                if (target.inspector != null && target.inspector.type != null)
                {
                    return target.inspector.type.Plugs;
                }
                
                return new List<Plug>();
            }

            public IEnumerable getShapes(MappingPortPart target)
            {
                if (target.inspector != null && target.inspector.part != null)
                {
                    DynamicPart isDynamic =  target.inspector.part.GetComponent<DynamicPart>();
                    if (isDynamic != null)
                        return isDynamic.getShapeNames();
                }
                
                return new List<string>();
            }
        }

    }

#endif
}
