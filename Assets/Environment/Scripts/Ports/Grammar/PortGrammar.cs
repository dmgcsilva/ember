﻿using Sirenix.OdinInspector;

using UnityEngine;
using System.Linq;
using System.Collections.Generic;

[System.Serializable]
public struct PortConnectionRule
{
    [SerializeField, HideInInspector] private PortType portA;
    [SerializeField, HideInInspector] private PortType portB;
    [SerializeField, HideInInspector] private Plug aConnectedToB;
    [SerializeField, HideInInspector] private Plug bConnectedByA;
    [SerializeField, HideInInspector] private Plug bConnectedToA;
    [SerializeField, HideInInspector] private Plug aConnectedByB;

    public PortConnectionRule(PortType portA, PortType portB, Plug aConnectedToB, Plug bConnectedByA, Plug bConnectedToA, Plug aConnectedByB) : this()
    {
        this.PortA = portA;
        this.PortB = portB;
        this.AConnectedToB = aConnectedToB;
        this.BConnectedByA = bConnectedByA;
        this.BConnectedToA = bConnectedToA;
        this.AConnectedByB = aConnectedByB;
    }

    public PortType PortA { get => portA; private set => portA = value; }
    public PortType PortB { get => portB; private set => portB = value; }
    public Plug AConnectedToB { get => aConnectedToB; private set => aConnectedToB = value; }
    public Plug BConnectedByA { get => bConnectedByA; private set => bConnectedByA = value; }
    public Plug BConnectedToA { get => bConnectedToA; private set => bConnectedToA = value; }
    public Plug AConnectedByB { get => aConnectedByB; private set => aConnectedByB = value; }

    public PortConnectionRule getInverse()
    {
        return new PortConnectionRule(portB, portA, bConnectedToA, aConnectedByB, aConnectedToB, bConnectedByA);
    }
}

[CreateAssetMenu(fileName = "PortGrammar", menuName = "Procedural/Ports/Grammar", order = 1)]
public class PortGrammar : ScriptableObject {

    [SerializeField, HideInInspector]
    private List<PortConnectionRule> connectionRules;

    public List<PortConnectionRule> ConnectionRules
    {
        get => connectionRules != null ? new List<PortConnectionRule>(connectionRules) : new List<PortConnectionRule>();
        private set => connectionRules = value != null ? new List<PortConnectionRule>(value) : null;
    }



#if UNITY_EDITOR


    [System.NonSerialized, ShowInInspector, HideLabel, HideReferenceObjectPicker, EnableIf("@inspector != null && inspector.editMode")]
    private Inspector inspector;

    [OnInspectorGUI]
    private void updateInspector()
    {
        if (inspector == null)
            inspector = new Inspector(this);
    }

    [DisableIf("@inspector != null && inspector.editMode"), Button]
    private void edit()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = true;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void save()
    {
        inspector.reset();
        inspector.save();
        inspector.editMode = false;
    }

    [HorizontalGroup("Split")]
    [EnableIf("@inspector != null && inspector.editMode"), Button]
    private void cancel()
    {
        inspector.reset();
        inspector.sync();
        inspector.editMode = false;
    }



    private class Inspector
    {
        [HideInInspector]
        public bool editMode;

        [HideInInspector]
        public PortGrammar target;

        public Inspector(PortGrammar target)
        {
            this.editMode = false;
            this.target = target;
            reset();
            sync();
        }

        public void reset()
        {

        }

        public void sync()
        {
            connectionRules = target.ConnectionRules.Select(t => new _PortConnectionRule(t)).ToList();
        }

        public void save()
        {
            target.ConnectionRules = connectionRules.Select(t => t.convert()).ToList();
        }

        public List<_PortConnectionRule> connectionRules;

        public struct _PortConnectionRule
        {
            [BoxGroup("Ports")]
            
            [OnValueChanged("resetPlugA")]
            [HorizontalGroup("Ports/Split"), HideLabel]
            [ValueDropdown("@EnvironmentUtils.getAssetDropdown<PortType>()")]
            public PortType portA;

            [OnValueChanged("resetPlugB")]
            [HorizontalGroup("Ports/Split"), HideLabel]
            [ValueDropdown("@EnvironmentUtils.getAssetDropdown<PortType>()")]
            public PortType portB;

            [BoxGroup("Plugs")]

            [HorizontalGroup("Plugs/A"), HideLabel, SuffixLabel("Connected To")]
            [EnableIf("@portA != null && portB != null")]
            [ValueDropdown("@portA != null ? portA.Plugs : new List<Plug>()")]
            public Plug aConnectedToB;

            [HorizontalGroup("Plugs/A"), HideLabel]
            [EnableIf("@portA != null && portB != null")]
            [ValueDropdown("@portB != null ? portB.Plugs : new List<Plug>()")]
            public Plug bConnectedByA;

            [HorizontalGroup("Plugs/B"), HideLabel, SuffixLabel("Connected By")]
            [EnableIf("@portA != null && portB != null")]
            [ValueDropdown("@portA != null ? portA.Plugs : new List<Plug>()")]
            public Plug aConnectedByB;

            [HorizontalGroup("Plugs/B"), HideLabel]
            [EnableIf("@portA != null && portB != null")]
            [ValueDropdown("@portB != null ? portB.Plugs : new List<Plug>()")]
            public Plug bConnectedToA;

            public _PortConnectionRule(PortConnectionRule target)
            {
                this.portA = target.PortA;
                this.portB = target.PortB;
                this.aConnectedToB = target.AConnectedToB;
                this.bConnectedByA = target.BConnectedByA;
                this.bConnectedToA = target.BConnectedToA;
                this.aConnectedByB = target.AConnectedByB;
            }

            public PortConnectionRule convert()
            {
                return new PortConnectionRule(
                    this.portA,
                    this.portB,
                    this.aConnectedToB,
                    this.bConnectedByA,
                    this.bConnectedToA,
                    this.aConnectedByB
                );
            }

            public void resetPlugA()
            {
                aConnectedToB = default;
                aConnectedByB = default;
            }

            public void resetPlugB()
            {
                bConnectedByA = default;
                bConnectedToA = default;
            }
        }

    }

#endif
}