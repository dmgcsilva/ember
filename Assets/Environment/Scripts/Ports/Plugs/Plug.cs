﻿using UnityEngine;

[CreateAssetMenu(fileName = "Plug", menuName = "Procedural/Ports/Plug", order = 1)]
public class Plug : ScriptableObject{}
