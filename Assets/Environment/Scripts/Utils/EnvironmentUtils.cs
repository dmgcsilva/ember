﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;

public class EnvironmentUtils
{
    public const string partsPath = "Assets/Environment/Resources/Parts/";

    public static IEnumerable getAllModuleParts(GameObject module)
    {
        foreach (Transform part in module.transform.Find("Parts"))
        {
            yield return part.gameObject;
        }
    }

    public static IEnumerable getAllPartElements(GameObject part)
    {
        foreach (Transform element in part.transform)
        {
            yield return element.gameObject;
        }
    }

    public static bool isPartDynamic(GameObject part)
    {
        if (part == null)
            return false;

        DynamicPart dynamicPart = part.GetComponent<DynamicPart>();

        if (dynamicPart != null)
            return true;
        else
            return false;
    }

    public static IEnumerable getPartShapes(GameObject part)
    {
        if (part == null)
            return new List<string>();

        DynamicPart dynamicPart = part.GetComponent<DynamicPart>();

        if (dynamicPart != null)
            return dynamicPart.getShapeNames();
        else
            return new List<string>();
    }


#if UNITY_EDITOR
    public static IEnumerable<ValueDropdownItem> getAssetDropdown<T>()
        where T : UnityEngine.Object
    {
        return UnityEditor.AssetDatabase.FindAssets("t:" + typeof(T).Name)
            .Select(x => UnityEditor.AssetDatabase.GUIDToAssetPath(x))
            .Select(x => new ValueDropdownItem(x.Split('/').Last().Split('.').First(), UnityEditor.AssetDatabase.LoadAssetAtPath<T>(x)));
    }

    public static IEnumerable getAssetsInPath(string path)
    {
        return UnityEditor.AssetDatabase.GetAllAssetPaths()
            .Where(x => x.StartsWith(path))
            .Select(x => x.Substring(path.Length))
            .Select(x => new ValueDropdownItem(x, UnityEditor.AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path + x)));
    }
#endif

}
